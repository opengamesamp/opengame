//Aici sunt toate textdraw-urile de player!

	SpeedometerTD[playerid] = CreatePlayerTextDraw(playerid, 569.556213, 302.995452, "~y~0~n~~b~km/h");
	PlayerTextDrawLetterSize(playerid, SpeedometerTD[playerid], 0.400000, 1.600000);
	PlayerTextDrawAlignment(playerid, SpeedometerTD[playerid], 2);
	PlayerTextDrawColor(playerid, SpeedometerTD[playerid], -1);
	PlayerTextDrawSetShadow(playerid, SpeedometerTD[playerid], 1);
	PlayerTextDrawSetOutline(playerid, SpeedometerTD[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, SpeedometerTD[playerid], 255);
	PlayerTextDrawFont(playerid, SpeedometerTD[playerid], 3);
	PlayerTextDrawSetProportional(playerid, SpeedometerTD[playerid], 1);
	PlayerTextDrawSetShadow(playerid, SpeedometerTD[playerid], 1);

	LOGOSERVER[playerid][0] = CreatePlayerTextDraw(playerid, 636.666931, 422.462371, "~b~Open~w~Game");
	PlayerTextDrawLetterSize(playerid, LOGOSERVER[playerid][0], 0.650221, 2.550755);
	PlayerTextDrawAlignment(playerid, LOGOSERVER[playerid][0], 3);
	PlayerTextDrawColor(playerid, LOGOSERVER[playerid][0], -1);
	PlayerTextDrawSetShadow(playerid, LOGOSERVER[playerid][0], 1);
	PlayerTextDrawSetOutline(playerid, LOGOSERVER[playerid][0], 0);
	PlayerTextDrawBackgroundColor(playerid, LOGOSERVER[playerid][0], 255);
	PlayerTextDrawFont(playerid, LOGOSERVER[playerid][0], 3);
	PlayerTextDrawSetProportional(playerid, LOGOSERVER[playerid][0], 1);
	PlayerTextDrawSetShadow(playerid, LOGOSERVER[playerid][0], 1);

	LOGOSERVER[playerid][1] = CreatePlayerTextDraw(playerid, 636.222351, 414.497680, "~r~NAME`/");
	PlayerTextDrawLetterSize(playerid, LOGOSERVER[playerid][1], 0.290666, 1.226666);
	PlayerTextDrawAlignment(playerid, LOGOSERVER[playerid][1], 3);
	PlayerTextDrawColor(playerid, LOGOSERVER[playerid][1], -1);
	PlayerTextDrawSetShadow(playerid, LOGOSERVER[playerid][1], 1);
	PlayerTextDrawSetOutline(playerid, LOGOSERVER[playerid][1], 0);
	PlayerTextDrawBackgroundColor(playerid, LOGOSERVER[playerid][1], 255);
	PlayerTextDrawFont(playerid, LOGOSERVER[playerid][1], 3);
	PlayerTextDrawSetProportional(playerid, LOGOSERVER[playerid][1], 1);
	PlayerTextDrawSetShadow(playerid, LOGOSERVER[playerid][1], 1);